
Bu repo-ya "ec2_public_key.pem" ve "ec2_private_key.pem" fayllari yuklenmeyib.
Asagidaki emrlerden istifade edib ozunuz key-pair yarada bilersiz. 

mkdir keys
cd keys
ssh-keygen -b 2048 -t rsa -m PEM -q -N "" -f ec2_key
mv ec2_key ec2_private_key.pem
mv ec2_key.pub ec2_public_key.pem

DIQQET: keys qovlugu terraform (.tf) fayllarinin oldugu yerde olmalidir.
