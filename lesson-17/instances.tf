# LATEST AMAZON LINUX IMAGE
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*-hvm-*"]
  }
}

# EC2 KEY PAIR
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair

resource "aws_key_pair" "ec2_key" {
  key_name   = "${var.prefix}-${var.ec2_key_name}"
  public_key = file("${path.module}/keys/ec2_public_key.pem") # Key pair created by "ssh-keygen -b 2048 -t rsa -m PEM -f key_file"

  tags = {
    Name = "${var.prefix}-${var.ec2_key_name}"
  }
}

# PUBLIC EC2 INSTANCE
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance

resource "aws_instance" "public_server" {
  ami                         = data.aws_ami.amazon_linux.id
  instance_type               = "t2.micro"
  vpc_security_group_ids      = [aws_security_group.public_sg.id]
  key_name                    = "${var.prefix}-${var.ec2_key_name}"
  subnet_id                   = aws_subnet.public_subnet.id
  availability_zone           = var.public_subnet_az
  associate_public_ip_address = true

  # SSH connections settings
  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file("${path.module}/keys/ec2_private_key.pem")
    host        = self.public_ip
  }

  # Copy private key to the EC2 instance
  provisioner "file" {
    source      = "${path.module}/keys/ec2_private_key.pem"
    destination = "/home/ec2-user/.ssh/ec2_private_key.pem"
  }

  # Change permission of the key file
  provisioner "remote-exec" {
    inline = [
      "chmod 600 /home/ec2-user/.ssh/ec2_private_key.pem",
    ]
  }

  tags = {
    Name = "${var.prefix}-${var.public_server_name}"
  }
}

# PRIVATE EC2 INSTANCE

resource "aws_instance" "private_server" {
  ami                         = data.aws_ami.amazon_linux.id
  instance_type               = "t2.micro"
  vpc_security_group_ids      = [aws_security_group.private_sg.id]
  key_name                    = "${var.prefix}-${var.ec2_key_name}"
  subnet_id                   = aws_subnet.private_subnet.id
  availability_zone           = var.private_subnet_az
  associate_public_ip_address = false

  tags = {
    Name = "${var.prefix}-${var.private_server_name}"
  }
}



