# AWS PROVIDER CONFIGURATION

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Company    = "IngressAcademy"
      CourseName = "AWS-and-Terraform"
      Lesson     = var.lesson
      ManagedBy  = "Terraform"
    }
  }
}

