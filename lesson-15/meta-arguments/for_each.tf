resource "aws_iam_user" "iam_admin_users" {
  # Using for_each and toset function
  for_each = toset(["Bob", "Alice", "James", "Ali"])
  name     = each.key
}

