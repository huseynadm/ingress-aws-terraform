data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*-hvm-*"]
  }
}

resource "aws_instance" "ec2_server" {
  count = 2

  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = ["sg-075900582aa3dffa9"]
  key_name               = "ec2-key-pem"

  tags = {
    Name = "server-${count.index + 1}"
  }
}

