# Get the latest Amazon Linux AMI

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel*-hvm-*"]
  }
}

# Create an EC2 instance

resource "aws_instance" "ec2_server" {
  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = ["sg-075900582aa3dffa9"]
  key_name               = "ec2-key-pem"

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file("ec2-key-pem.pem")
    host        = self.public_ip
  }

  provisioner "file" {
    source      = "testfile.txt"
    destination = "/tmp/test_file.txt"
  }

  provisioner "remote-exec" {
    inline = [
      "echo 'Hello from Terraform' > /tmp/remote_exec_hello.txt",
    ]
  }

  provisioner "local-exec" {
    command = "echo ${self.private_ip} > ec2_server_private_ip.txt"
  }
}

