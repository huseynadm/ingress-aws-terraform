
resource "aws_instance" "webapp" {
  ami                    = "ami-12345678"
  instance_type          = "t2.micro"
  vpc_security_group_ids = ["vpc-12345678"]
  key_name               = "ec2-key-pem"
  availability_zone      = "us-east-1a"

  user_data = <<EOF
    #!/bin/bash
    yum install -y httpd
    echo "<h1>Deployed via Terraform</h1>" > /var/www/html/index.html
    systemctl enable httpd
    systemctl start httpd
  EOF

  tags = {
    Name      = "webapp"
    CreatedBy = "Terraform"
  }
}

