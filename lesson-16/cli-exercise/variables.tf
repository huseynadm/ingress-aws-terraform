# Variables

variable "availability_zone" {
  default = ["us-east-1a"]
}

variable "instance_name" {
  type = string
}

variable "security_group_name" {
  type = string
}

variable "vpc_id" {
}

variable "my_ip" {
}

